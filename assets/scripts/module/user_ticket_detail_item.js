/*
    出票详情ITEM
    user_ticket_detail_item
*/
cc.Class({
    extends: cc.Component,

    properties: {
        labContent:{
            default: null,
            type: cc.Label
        },

        labContent1:{
            default: null,
            type: cc.Label
        },

        rtContent:{
            default: null,
            type: cc.RichText
        },

        ndSp:{
            default:null,
            type:cc.Node
        },

        ndContent:{
            default:null,
            type:cc.Node
        },

        _data:null,
    },

    // use this for initialization
    onLoad: function () {
        this.onShow();
    },

    init: function(data){
        this._data = data;
    },

    onShow:function(){
        if(this._data == null)
            return;
        var dec1 = "<color=#000000>出票状态：</color>";
        var dec2 = "<color=#000000>中奖金额：</color>";
        var brStr = "<br/>";
        var redDec1 = "<color=#ff0707>";
        var redDec2 = "  </color>";
        var colDec1 = "<color=#7a7a7a>";
        var colDec2 = "元</color>";
        var blaDec1 = "<color=#000000>";
        var blaDec2 = "  </color>";
        var lbDec1 = "【";
        var lbDec2 = "】";
        var dec3 = "选号详情：";
        var dec4 = "电子票号：";
        var isend = true; 
        var rtcontent = "";
        var dec11 = LotteryUtils.getTicketStatus(this._data.TicketStatus); 
        if(this._data.TicketStatus == 3 || this._data.TicketStatus == 4)
        {
            rtcontent += dec1+colDec1+dec11+colDec2;
            isend = false;
        }
        else if(this._data.TicketStatus == 0)
        {
            rtcontent += dec1+blaDec1+dec11+blaDec2;
        }
        else
            rtcontent += dec1+redDec1+dec11+redDec2;

        if(isend)
        {
            if(this._data.WinMoney == 0)
            {
                rtcontent += brStr+dec2+colDec1+LotteryUtils.moneytoServer(this._data.WinMoney)+colDec2;
            }
            else
            {
                rtcontent += brStr+dec2+redDec1+LotteryUtils.moneytoServer(this._data.WinMoney)+redDec2;
            } 
        }
        this.rtContent.string = rtcontent;
        var lbContent = "";
        lbContent += dec3 + lbDec1 + LotteryUtils.getPlayTypeDecByPlayId(this._data.PlayCode) +lbDec2
        + "  " +this._data.Number + " "+ this._data.Multiple + "倍" ;
        this.labContent.string = lbContent;
        var lbContent1 = dec4 + this._data.Tickets;
        this.labContent1.string = lbContent1;
        this.ndContent.getComponent(cc.Layout)._updateLayout();
        this.ndSp.height = this.ndContent.height+80;
        this.node.height = this.ndSp.height+10;
    }

});
