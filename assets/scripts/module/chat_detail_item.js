/**
 * !#zh 投注订单详情组件
 * @information 投注号码，投注方式、几注几倍、订单所需钱数
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labDec:{
            default:null,
            type:cc.Label
        },

        rtNumbers:{
            default:null,
            type:cc.RichText
        },

        labNumbers:{
            default:null,
            type:cc.Label
        },

        _data:null,
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.labDec.string = this._data.dec;
            //this.rtNumbers.string = this._data.number;
            this.labNumbers.string = this._data.number;
        }
    },

    /** 
    * 接收投注订单详情数据
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    },

});
