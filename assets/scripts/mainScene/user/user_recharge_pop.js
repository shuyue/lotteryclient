/*
充值中心界面
*/ 
cc.Class({
    extends: cc.Component,

    properties: {
        labName: {
            default: null,
            type: cc.Label
        },

        labBalance: {
            default: null,
            type: cc.Label
        },

        labMoney: {
            default: null,
            type: cc.Label
        },

        ndSpSelBg:{
            default:null,
            type:cc.Node
        },

        rechargeSuccess:{
            default: null,
            type: cc.Prefab
        },

        curSelToggle:cc.Toggle,

        _payMoney:"",
        _selectMoneyIndex:null
    },

    // use this for initialization
    onLoad: function () {
        this.labMoney.string = "20";
        this.labName.string = User.getNickName();
        this.labBalance.string = User.getBalance().toString() + "元";
        this._money = "";
    },

    //选择要充值的金额 
    onSelectMoneyBtn: function(toggle, customEventData){
        this.labMoney.string = customEventData;
        this.curSelToggle = toggle;
        this.ndSpSelBg.active = false;
    },

    onDidRetMoney: function(editbox,customEventData) {
        if(!Utils.isInt(editbox.string))
        {
             editbox.string = this._payMoney;
             return;
        }
        var num = parseInt(editbox.string)
        
        this._payMoney = num.toString();
        this.labMoney.string = num.toString();
    },

    onDidBegan:function(){
        this.ndSpSelBg.active = true;
        this.curSelToggle.getComponent(cc.Toggle).isChecked = false;
    },

    onTextChangeMoney: function(text,editbox,customEventData) {
         var txt = text;
        if(Utils.isInt1(txt) || txt == "")
            this._money = txt;
        else
        {
            txt = this._money;
            editbox.string = txt;
            return;
        }
        
        var num = parseInt(editbox.string);
        if(num >= 5 )//
        {
            this._payMoney = num.toString();
            this.labMoney.string = num.toString();
        }
        else
        {
            if(txt == "")
            {
                this._payMoney = "0";
                this.labMoney.string = this._payMoney;
                editbox.string = this._payMoney;
            }
            else
            {
                this._payMoney = "5";
                this.labMoney.string = this._payMoney;
                editbox.string = this._payMoney;
            }
        }
    },

    //微信支付
    onWeiChatPay: function()
    {
        ComponentsUtils.showTips("暂时不支持微信支付！");
    },

    //支付宝支付
    onAliPay: function()
    {
        ComponentsUtils.showTips("暂时不支持支付宝支付！");
    },

    onClose: function(){
         this.node.getComponent("Page").backAndRemove();
    },

});
