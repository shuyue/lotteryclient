/**
 * 充值成功界面
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labMoney:{
            default:null,
            type:cc.Label
        },
        
        labTime:{
            default:null,
            type:cc.Label
        }
    },

    // use this for initialization
    onLoad: function () {
        
    },

    /** 
    * 接收充值信息并显示
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this.labMoney.string = data+"元";
        this.labTime.string = Utils.getNowFormatDate();
    },

    onClose:function(){
        window.Notification.emit("USER_useruirefresh","");
        this.node.getComponent("Page").backAndRemove();
    }
});
